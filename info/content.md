Forgetfulness is a typical problem for elderly people suffering from MCI.
For instance, leaving the house they tend to forget shutting windows, balcony
doors, leaving open refrigerator doors or lights switched on. Detection of such
hazards can be handled by equipping the home with a multitude of sensors,
however elderly people usually dislike any modifications to their homes and
subsequent maintenance of the resulting system. Thus a different remedy can
be recommended. A robot can detect all potential risks by exploring the home.

The scenario is the following. Upon request from the user or executing
a scheduled task the robot searches the house, locating all hazards and subsequently reports them to the user, ensuring his or her safety, thus supporting
independent living.
